<?php
/**
 * @file
 * PHP Error Module
 */

/**
 * Define PHP Error Constants
 */
define('PHP_ERROR_APPLICATION_ROOT',        @$_SERVER['DOCUMENT_ROOT']);
define('PHP_ERROR_SERVER_NAME',             @$_SERVER['SERVER_NAME']);
define('PHP_ERROR_ERROR_REPORTING_ON',      'E_ALL|E_STRICT');
define('PHP_ERROR_ERROR_REPORTING_OFF',     -1);
define('PHP_ERROR_FILE_LINK_FORMAT',        NULL);
define('PHP_ERROR_APPLICATION_FOLDERS',     NULL);
define('PHP_ERROR_IGNORE_FOLDERS',          NULL);
define('PHP_ERROR_CLEAR_ALL_BUFFERS',       1);
define('PHP_ERROR_CATCH_AJAX_ERRORS',       1);
define('PHP_ERROR_CATCH_SUPRESSED_ERRORS',  0);
define('PHP_ERROR_CATCH_CLASS_NOT_FOUND',   1);
define('PHP_ERROR_SNIPPET_NUM_LINES',       13);
define('PHP_ERROR_BACKGROUND_TEXT',         '');

/**
 * Helper function to retrieve PHP Error options.
 *
 * @param string $name
 *   The name of the option to return.
 * @param $fallback
 *   (optional) The fall back data to return if no php.ini value is set.
 *
 * @return
 *   The Drupal setting, php.ini configuration option or $fallback data provided.
 *   In that order.
 */
function php_error_get_option($name, $fallback = NULL) {
  $options = &drupal_static(__FUNCTION__ . '_options');
  if (!isset($options)) {
    $options = array();
  }
  if (!isset($options[$name])) {
    $variable = variable_get('php_error_' . $name, NULL);
    if (is_null($variable)) {
      if ($ini = get_cfg_var('php_error.' . $name)) {
        $options[$name] = $ini;
      }
      else {
        $options[$name] = $fallback;
      }
    }
    else {
      $options[$name] = $variable;
    }
    switch ($name) {
      case 'error_reporting_on':
        if (is_array($options[$name])) {
          $options[$name] = implode('|', $options[$name]);
        }
        break;
    }
  }
  return $options[$name];
}

/**
 * Implements hook_boot().
 */
function php_error_boot() {
  global $_php_error, $_php_error_already_setup, $_php_error_global_handler;
  $_php_error = FALSE;
  if (!variable_get('php_error_enable', 0)) {
    // PHP Error can be automatically loaded via setting the auto_prepend_file
    // value in the local environment's php.ini configuration file. If php_error
    // is already loaded and this Drupal site has disabled it, the existing
    // handler should be turned off.
    // @see https://github.com/JosephLenton/PHP-Error/wiki/Example-Setup
    // @see https://github.com/JosephLenton/PHP-Error/wiki/API
    if (!empty($_php_error_already_setup) && !empty($_php_error_global_handler)) {
      global $_php_error_is_ini_enabled;
      $_php_error_is_ini_enabled = FALSE;
      $_php_error_global_handler->turnOff();
      ob_end_flush();
    }
    // Ensure the PHP error and exception handlers are set back to Drupal.
    // @see _drupal_bootstrap_configuration()
    @ini_set('html_errors', TRUE);
    set_error_handler('_drupal_error_handler');
    set_exception_handler('_drupal_exception_handler');
    return;
  }
  // Attempt to include the PHP Error library if it doesn't already exist.
  // See explaination above.
  if (!class_exists('\php_error\ErrorHandler')) {
    $path = module_exists('libraries') ? libarary_get_path('php_error') : FALSE;
    if ($path && file_exists($path . '/php_error.php')) {
      include $path . '/php_error.php';
    }
    elseif ($path && file_exists($path . '/src/php_error.php')) {
      include $path . '/src/php_error.php';
    }
  }
  // Always create a new instance of PHP Error with properly configured options.
  if (class_exists('\php_error\ErrorHandler')) {
    // Set basic options
    $options = array(
      'application_root'    => php_error_get_option('application_root',     PHP_ERROR_APPLICATION_ROOT),
      'server_name'         => php_error_get_option('server_name',          PHP_ERROR_SERVER_NAME),
      'error_reporting_off' => php_error_get_option('error_reporting_off',  PHP_ERROR_ERROR_REPORTING_OFF),
      'error_reporting_on'  => php_error_get_option('error_reporting_on',   PHP_ERROR_ERROR_REPORTING_ON),
      'ignore_folders'      => php_error_get_option('ignore_folders',       PHP_ERROR_IGNORE_FOLDERS),
      'application_folders' => php_error_get_option('application_folders',  PHP_ERROR_APPLICATION_FOLDERS),
    );
    // Set non-cli options.
    if (!drupal_is_cli()) {
      $options['clear_all_buffers']       = php_error_get_option('clear_all_buffers',       PHP_ERROR_CLEAR_ALL_BUFFERS);
      $options['catch_ajax_errors']       = php_error_get_option('catch_ajax_errors',       PHP_ERROR_CATCH_AJAX_ERRORS);
      $options['catch_supressed_errors']  = php_error_get_option('catch_supressed_errors',  PHP_ERROR_CATCH_SUPRESSED_ERRORS);
      $options['catch_class_not_found']   = php_error_get_option('catch_class_not_found',   PHP_ERROR_CATCH_CLASS_NOT_FOUND);
      $options['snippet_num_lines']       = php_error_get_option('snippet_num_lines',       PHP_ERROR_SNIPPET_NUM_LINES);
      $options['background_text']         = php_error_get_option('background_text',         PHP_ERROR_BACKGROUND_TEXT);
      $options['file_link_format']        = php_error_get_option('file_link_format',        PHP_ERROR_FILE_LINK_FORMAT);
    }
    $_php_error = new \php_error\ErrorHandler($options);
    $_php_error->turnOn();
  }
}

/**
 * Implements hook_menu().
 */
function php_error_menu() {
  $items = array();
  $items['admin/config/development/php_error'] = array(
    'title' => 'PHP Error',
    'description' =>  'Provides configuration options for PHP Error.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('php_error_admin_settings'),
    'access arguments' => array('administer site configuration'),
  );
  return $items;
}

/**
 * PHP Error Admin Settings Callback
 */
function php_error_admin_settings($form, $form_state) {
  $form = array();
  $form['php_error_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Imporved PHP Error Reporting'),
    '#default_value' => variable_get('php_error_enable', 0),
    '#description' => t(''),
  );
  $form['php_error_options'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => 'Options',
    '#states' => array(
      'visible' => array(
        ':input[name="php_error_enable"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['php_error_options']['php_error_error_reporting_off'] = array(
    '#type' => 'radios',
    '#title' => 'Error Reporting Off value <em>default: -1 (everything)</em>',
    '#options' => array(-1 => t('Everything'), 0 => t('Select')),
    '#desription' => t('The error reporting value for when errors are turned on by PHP Error.'),
    '#default_value' => php_error_get_option('error_reporting_off', PHP_ERROR_ERROR_REPORTING_OFF),
  );
  $form['php_error_options']['php_error_error_reporting_on'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Error Reporting On value',
    '#description' => t('The error reporting value for when PHP Error reporting is turned off. By default it just goes back to the standard level. Descriptions can be found here <a href="http://www.php.net/manual/en/errorfunc.constants.php">http://www.php.net/manual/en/errorfunc.constants.php</a>'),
    '#options' => drupal_map_assoc(array(
      t('E_ERROR'),
      t('E_WARNING'),
      t('E_PARSE'),
      t('E_NOTICE'),
      t('E_CORE_ERROR'),
      t('E_CORE_WARNING'),
      t('E_COMPILE_ERROR'),
      t('E_COMPILE_WARNING'),
      t('E_USER_ERROR'),
      t('E_USER_WARNING'),
      t('E_USER_NOTICE'),
      t('E_STRICT'),
      t('E_RECOVERABLE_ERROR'),
      t('E_DEPRECATED'),
      t('E_USER_DEPRECATED'),
      t('E_ALL'))),
    '#default_value' => explode('|', php_error_get_option('error_reporting_on', PHP_ERROR_ERROR_REPORTING_ON)),
    '#states' => array(
      'visible' => array(
        ':input[name="php_error_error_reporting_off"]' => array('value' => 0),
      ),
    ),
  );
  $form['php_error_options']['php_error_clear_all_buffers'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clear All Buffers'),
    '#description' => t('When on, this will help prevent dying on a white screen. Strongly recommended if using the devel module.'),
    '#default_value' => php_error_get_option('clear_all_buffers', PHP_ERROR_CLEAR_ALL_BUFFERS),
  );
  $form['php_error_options']['php_error_catch_ajax_errors'] = array(
    '#type' => 'checkbox',
    '#title' => t('Catch AJAX Errors'),
    '#description' => t('When on, this will inject JS Ajax wrapping code, to allow this to catch any future JSON errors.'),
    '#default_value' => php_error_get_option('catch_ajax_errors', PHP_ERROR_CATCH_AJAX_ERRORS),
  );
  $form['php_error_options']['php_error_catch_supressed_errors'] = array(
    '#type' => 'checkbox',
    '#title' => t('Catch Supressed Errors (<em>default: false</em>)'),
    '#description' => t('The @ supresses errors. If set to true, then they are still reported anyway, but respected when false.'),
    '#default_value' => php_error_get_option('catch_supressed_errors', PHP_ERROR_CATCH_SUPRESSED_ERRORS),
  );
  $form['php_error_options']['php_error_catch_class_not_found'] = array(
    '#type' => 'checkbox',
    '#title' => t('Catch Class Not Found (<em>default: true</em>)'),
    '#description' => t('When true, loading a class that does not exist will be caught. If there are any existing class loaders, they will be run first, giving you a chance to load the class.'),
    '#default_value' => php_error_get_option('catch_class_not_found', PHP_ERROR_CATCH_CLASS_NOT_FOUND),
  );
  $form['php_error_options']['php_error_file_link_format'] = array(
    '#type' => 'textfield',
    '#title' => t('File Link Format'),
    '#description' => t('Allow filenames to be wrapped with a formatted URL. Use the following tokens: %f = filename, %l = line number. For example, if using TextMate you can enter the following: txmt://open?url=file://%f&line=%l'),
    '#default_value' => php_error_get_option('file_link_format', PHP_ERROR_FILE_LINK_FORMAT),
  );
  $form['php_error_options']['php_error_application_root'] = array(
    '#type' => 'textfield',
    '#title' => t('Application Root (<em>default: $_SERVER[\'DOCUMENT_ROOT\']	</em>)'),
    '#description' => t('When it\'s working out the stack trace, this is the root folder of the application, to use as it\'s base. A relative path can be given, but lets be honest, an explicit path is the way to guarantee that you will get the path you want. My relative might not be the same as your relative.<br /><strong>enter a comma seperated list of folders</strong>'),
    '#default_value' => php_error_get_option('application_root', PHP_ERROR_APPLICATION_ROOT),
  );
  $form['php_error_options']['php_error_snippet_num_lines'] = array(
    '#type' => 'textfield',
    '#title' => t('Snippet Number of Lines (<em>default: 13</em>)'),
    '#description' => t('The number of lines to display in the code snippet. That includes the line being reported.'),
    '#default_value' => php_error_get_option('snippet_num_lines', PHP_ERROR_SNIPPET_NUM_LINES),
  );
  $form['php_error_options']['php_error_server_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Server Name (<em>default: $_SERVER[\'SERVER_NAME\']</em>)'),
    '#description' => t('The name for this server; it\'s domain address or ip being used to access it. This is displayed in the output to tell you which project the error is being reported in.'),
    '#default_value' => php_error_get_option('server_name', PHP_ERROR_SERVER_NAME),
  );
  $form['php_error_options']['php_error_ignore_folders'] = array(
    '#type' => 'textfield',
    '#title' => t('Ignore Folders (<em>default: NULL (no folders)</em>)'),
    '#description' => t('This is allows you to highlight non-framework code in a stack trace. An array of folders to ignore, when working out the stack trace. This is folder prefixes in relation to the application_root, whatever that might be. They are only ignored if there is a file found outside of them. If you still don\'t get what this does, don\'t worry, it\'s here cos I use it.<br /><strong>enter a comma seperated list of folders</strong>'),
    '#default_value' => php_error_get_option('ignore_folders', PHP_ERROR_IGNORE_FOLDERS),
  );
  $form['php_error_options']['php_error_application_folders'] = array(
    '#type' => 'textfield',
    '#title' => t('Application Folders (<em>default: NULL (no folders)</em>)'),
    '#description' => t('Just like ignore, but anything found in these folders takes precedence over anything else.<br /><strong>enter a comma seperated list of folders</strong>'),
    '#default_value' => php_error_get_option('application_folders', PHP_ERROR_APPLICATION_FOLDERS),
  );
  $form['php_error_options']['php_error_background_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Background Text (<em>default: an empty string</em>)'),
    '#description' => t('The text that appeares in the background. By default this is blank. Why? You can replace this with the name of your framework, for extra customization spice.'),
    '#default_value' => php_error_get_option('background_text', PHP_ERROR_BACKGROUND_TEXT),
  );
  $form['actions']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
    '#weight' => 15,
    '#submit' => array('php_error_options_form_reset'),
  );
  return system_settings_form($form);
}

function php_error_options_form_reset($form, &$form_state) {
  form_state_values_clean($form_state);
  foreach ($form_state['values'] as $key => $value) {
    variable_del($key);
  }
}
